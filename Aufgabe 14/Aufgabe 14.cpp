// Aufgabe 14.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Vektor.h"


int main()
{
	Vektor* v = new Vektor(10);
	v->set(0, 0);
	v->set(1, 1);
	v->set(2, 2);
	v->set(3, 3);
	v->set(4, 4);
	v->set(5, 5);
	v->set(6, 6);
	v->set(7, 7);
	v->set(8, 8);
	v->set(9, 9);

	printf("%d", v->bin_suche(3));


    return 0;
}

