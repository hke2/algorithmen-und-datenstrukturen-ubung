// Aufgabe 13.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <chrono>
#include <iostream>


/*recursiv:
36: 1161899µs
41: 12312905µs
44: 51s

/*iterativ:
2310879: 1s 
23394330: 10s
140453151: 1min
*/

long fibonacci_rec(int n)
{
	if (n <= 1)
		return 1;
	else 
		return fibonacci_rec(n - 1) + fibonacci_rec(n - 2);
}

long fibonacci_it()
{
	int n = 0;
	int a = 1;
	int b = 1;
	long c;

	auto start = std::chrono::high_resolution_clock::now();

	while (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count() < 60000000) {
		c = a + b;
		a = b;
		b = c;
		n++;
	}
	return c;
}

int main()
{
	long fib_it = fibonacci_it();

	auto start = std::chrono::high_resolution_clock::now();
	long fib = fibonacci_rec(44);
	auto end = std::chrono::high_resolution_clock::now();


	auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
	std::cout << microseconds.count() << "µs\n";
	getchar();

    return 0;
}

