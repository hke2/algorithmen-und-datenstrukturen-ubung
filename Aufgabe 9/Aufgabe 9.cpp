// Aufgabe 9.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>

void printIt(char* str) {
	for (int i = 0; i <= strlen(str)-1; i++) {
		printf("%c", str[i]);
	}
}

void printRec(char* str, int pos) {
	if (pos > 0) {
		printRec(str, pos - 1);
	}
	printf("%c", str[pos]);
}

int main()
{
	char str[] = "foobar123";
	printIt(str);
	printf("\n");

	printRec(str, strlen(str) - 1);

	getchar();

    return 0;
}

