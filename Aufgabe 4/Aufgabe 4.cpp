// Aufgabe 4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int zug; // 2^n -1

void bewege(int i, int a, int b, int c) {
	if(i > 0) {
		bewege(i - 1, a, c, b);
		zug++;
		printf("%d:\tbewegt oberste Scheibe von %d nach %d\n", zug, a, c);
		bewege(i - 1, b, a, c);
	}
}
int main()
{
	printf("Anzahl Scheiben: ");
	int anzScheiben;
	scanf_s("%d", &anzScheiben); fflush(stdin); getchar();

	bewege(anzScheiben, 1, 3, 2); // von 1 nach 3, 3 Scheiben

	getchar();

    return 0;
}

