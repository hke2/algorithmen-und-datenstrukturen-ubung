// Aufgabe 16.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Stack.h"

int main()
{
	int val1 = 1;
	int val2 = 2;
	int val3 = 3;

	Stack<int>* s = new Stack<int>();
	s->push(val1);
	s->push(val2);
	s->push(val3);

	printf("length: %d\n", s->length());

	printf("%d\n", s->pop());
	printf("%d\n", s->pop());
	printf("%d\n", s->pop());

	getchar();


    return 0;
}

