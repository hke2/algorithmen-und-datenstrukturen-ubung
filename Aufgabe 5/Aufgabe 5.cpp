// Aufgabe 5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

/*
		1, falls n<=2
f(n)=	 n*f(n-1)-f(n-2) falls n > 2.
*/

int f_r(int n) {
	if (n <= 2)
		return 1;
	else
		return n * f_r(n - 1) - f_r(n - 2);
}

/*
1 = 1
2 = 1
3 = 2 == 3*1 -1
4 = 7 == 4*2 -1
5 = 33 == 5*7 -2
*/

int f_i(int n) {
	int re = 1;
	int a = 0;
	int b = 0;

	for (int i = 0; i <= n; i++) {
		if (i <= 2) {
			re = 1;
		}
		else {
			re = i * a - b;
		}

		b = a;
		a = re;
	}

	return re;
}

int main()
{
	int input;

	printf("Eingabe: ");
	scanf_s("%d", &input); fflush(stdin); getchar();

	printf("rekursiv: %d, iterativ: %d", f_r(input), f_i(input)); 
	getchar();

    return 0;
}

