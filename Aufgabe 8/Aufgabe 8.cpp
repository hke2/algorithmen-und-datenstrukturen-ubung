// Aufgabe 8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


double f(int stelle) {
	if (stelle == 20) {
		return 1 + double(stelle) / double(stelle * 2 + 1);
	}
	
	return 1 + double(stelle) / double(stelle * 2 + 1) * f(stelle + 1);
}

int main()
{
	double pi = 2.0f * f(1);
	printf("Die ersten Stellen von Pi: %.101f", pi);
	getchar();

    return 0;
}

