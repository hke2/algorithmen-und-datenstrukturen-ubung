#pragma once
template <class item_type>
class Stack
{
	struct node
	{
		item_type item;
		node* next;
	};
private:
	node * tail;
public:
	Stack() {
		this->tail = nullptr;
	};

	virtual ~Stack() {};

	/*void push(item_type &r) {
		node* n = new node();
		n->item = r;
		n->next = this->tail;

		this->tail = n;
	};*/

	void push(item_type r) {
		node* n = new node();
		n->item = r;
		n->next = this->tail;

		this->tail = n;
	};

	item_type pop() {
		if (tail != nullptr) {
			node * n = tail;
			tail = n->next;
			item_type t = n->item;
			delete n;
			return t;
		}
		else {
			return NULL;
		}
	};

	item_type top() {
		if (tail != nullptr) {
			return tail->item;
		}
		else {
			return NULL;
		}
	};

	int length() {
		node* n = tail;
		int counter = 0;

		while (n != nullptr) {
			counter++;
			n = n->next;
		}

		return counter;
	};

	bool empty() {
		return length() == 0;
	};
};