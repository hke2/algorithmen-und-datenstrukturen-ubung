// Aufgabe 18.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Stack.h"
#include <string.h>

int opPriority(char op) {
	switch (op)
	{
	case '+':
	case '-':
		return 1;

	case '*':
	case '/':
		return 2;

	case '^':
		return 3;
	}
	return -1;
}

void infixToPostfix(char* exp) {
	Stack<char>* s = new Stack<char>();
	int k = -1;

	for (int i = 0; exp[i]; i++) {
		if (exp[i] >= 48 && exp[i] <= 57) {
			exp[++k] = exp[i];
		}
		else if (exp[i] == '(') {
			s->push(exp[i]);
		}
		else if (exp[i] == ')') {
			while (!s->empty() && s->top() != '(') {
				exp[++k] = s->pop();
			}
			s->pop();
		}
		else {
			while (!s->empty() && opPriority(exp[i]) <= opPriority(s->top())) {
				exp[++k] = s->pop();
			}
			s->push(exp[i]);
		}
	}

	while (!s->empty()) {
		exp[++k] = s->pop();
	}

	exp[++k] = '\0';
	printf("%s", exp);
}


int main()
{
	char exp[] = "3*(1+2)";
	infixToPostfix(exp);

	getchar();

    return 0;
}

