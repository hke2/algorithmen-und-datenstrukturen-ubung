// Aufgabe 15.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

/*
d) n rekursive aufrufe
e) linear: O(n)
*/

int t_rec(int n) {
	if (n == 1)
		return 1;
	else
		return  n * t_rec(n - 1) + n;
}

int t_it(int n) {
	int value = 1;
	for (int i = 2; i <= n; i++) {
		value = i * value + i;
	}
	return value;
}

int main()
{
	printf("rekursiv: %d\n", t_rec(10));
	printf("iterativ: %d\n", t_it(10));
	getchar();
    return 0;
}

