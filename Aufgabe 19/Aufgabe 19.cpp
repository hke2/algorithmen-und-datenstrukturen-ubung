// Aufgabe 19.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Stack.h"

void postfixCalc(char* exp) {
	Stack<int>* s = new Stack<int>();

	for (int i = 0; exp[i]; i++) {
		if (exp[i] >= 48 && exp[i] <= 57) {
			s->push(exp[i] - 48);
		}
		else {
			int second = s->pop();
			int first = s->pop();
			switch (exp[i]){
			case '+':
				s->push(first + second);
				break;
			case '-':
				s->push(first - second);
				break;
			case '*':
				s->push(first * second);
				break;
			case '/':
				s->push(first / second);
				break;
			}
		}
	}

	if (s->length() == 1) {
		printf("%d", s->pop());
	}

}


int main()
{
	char exp[] = "48*8+42/+";
	postfixCalc(exp);
	getchar();
    return 0;
}

