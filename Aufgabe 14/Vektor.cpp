#include "stdafx.h"
#include "Vektor.h"


Vektor::Vektor(int dim)
{
	dimension = dim;
	daten = new int[dim];
}

Vektor::~Vektor()
{
}

void Vektor::set(int i, int val)
{
	daten[i] = val;
}

int Vektor::get(int i)
{
	return daten[i];
}

int Vektor::bin_suche(int sw)
{
	int l_u = 0, l_o = dimension - 1;
	while (l_u <= l_o) {
		int i = l_u + ((l_o - l_u) / 2);
		if (sw == daten[i]) {
			return i;
		}
		else if (sw < daten[i]) {
			l_o = i -1 ;
		}
		else {
			l_u = i +1;
		}
	}

	return 0;
}
