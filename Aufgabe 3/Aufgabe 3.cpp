// Aufgabe 3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int main()
{
	int n;
	scanf_s("%d", &n); fflush(stdin); getchar();

	do {
		if (n % 2 == 0) {
			n /= 2;
		}
		else {
			n = n * 3 + 1;
		}

		printf("%d\n", n);

	} while (n > 1);

	getchar();

    return 0;
}

