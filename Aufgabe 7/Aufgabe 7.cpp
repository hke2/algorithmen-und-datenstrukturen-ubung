// Aufgabe 7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

float harmonischeReihe_I(int n) {
	float re = 0.0f;

	for (float i = 1; i <= n; i++) {
		re += ((float)1 / i);
	}

	return re;
}

float harmonischeReihe_R(int n) {
	if (n == 1) {
		return 1.0f;
	}

	return (1.0f / (float)n) + harmonischeReihe_R(n - 1);
}

int main()
{
	int n;
	printf("n = ");
	scanf_s("%d", &n); fflush(stdin); getchar();

	printf("%f %f", harmonischeReihe_I(n), harmonischeReihe_R(n));
	getchar();

    return 0;
}

