#pragma once

template<class c>
class Table {
	struct node
	{
		c item;
		node* next;
		node* previous;
	};
private:
	node* head;
	node* tail;
	node* current;

public:
	Table() {
		head = nullptr;
		tail = nullptr;
		current = nullptr;
	};

	~Table() {};

	bool insert(c r) {
		if (!current) return false;

		node* n = new node();
		n->item = r;
		n->next = current;

		if (current->previous) {
			n->previous = current->previous;
			current->previous->next = n;
		}
		else {
			head = n;
		}

		current->previous = n;

		return true;
	};

	bool append(c r) {
		node* n = new node();
		n->item = r;
		n->previous = tail;
		n->next = nullptr;

		if (tail) tail->next = n;
		if (!head) head = n;
		tail = n;

		return true;
	};
	
	bool first() {
		if (head) {
			current = head;
			return true;
		}
		return false;		
	};

	bool last() {
		if (tail) {
			current = tail;
			return true;
		}
		return false;
	};

	bool next() {
		if (current->next) {
			current = current->next;
			return true;
		}
		return false;
	};

	bool previous() {
		if (current->previous) {
			current = current->previous;
			return true;
		}
		return false;
	}; 

	bool delete_node() {
		if (!current) return false;

		node* n = current;

		if (current->previous && current->next) {
			current->previous->next = current->next;
			current->next->previous = current->previous;
		}
		else if (current->previous) {
			current->previous->next = nullptr;
			tail = current->previous;
		}
		else if (current->next) {
			current->next->previous = nullptr;
			head = current->next;
		}
		else return false;

		delete n;
		return true;
	};

	bool get_node(c &r) {
		if (!current) return false;
		r = current->item;
		return true;
	};

	bool set_node(c &r) {
		if (!current) return false;
		current->item = r;
		return true;
	};
};