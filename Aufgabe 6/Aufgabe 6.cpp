// Aufgabe 6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int divider(int val1, int val2) {
	int m = val1;
	int n = val2;
	int r = 0;

	do {
		if (m < n) {
			int tmp = m;
			m = n;
			n = tmp;
		}

		r = m - n;
		m = n;
		n = r;
	} while (r != 0);

	return m;
}


int main()
{
	int val1;
	int val2;

	printf("Erste Zahl: ");
	scanf_s("%d", &val1); fflush(stdin); getchar();

	printf("Zweite Zahl: ");
	scanf_s("%d", &val2); fflush(stdin); getchar();

	printf("GGT: %d", divider(val1, val2));
	getchar();

    return 0;
}

